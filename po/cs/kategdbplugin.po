# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2012, 2013, 2014, 2017.
# Vit Pelcak <vit@pelcak.org>, 2021, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2023-05-31 11:35+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Příkaz GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Lokální aplikace"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Vzdálené TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Vzdálený sériový port"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Hostitel"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr ""

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Přidat nový cíl"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Zkopírovat cíl"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Smazat cíl"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Program:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Pracovní adresář:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "ID Procesu:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumenty:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr ""

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr ""

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Přesměrovat IO"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr ""

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Pokročilá nastavení"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Cíle"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Cíl %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Nastavení uživatelského ladicího adaptéru"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Soubor nastavení:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Nastavení výchozího ladicího adaptéru"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Ladicí program"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "Žádná data JSON k ověření."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "Data JSON jsou platná."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Data JSON jsou neplatná: žádný objekt JSON"

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Data JSON jsou neplatná: %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Místní"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Registry CPU"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Nebyl vybrán žádný ladicí nástroj. Prosím vyberte jeden v kartě 'Nastavení' "
"v panelu 'Ladit'."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr ""

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb bylo ukončeno normálně ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "všechna běžící vlákna"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr ""

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "zastaveno (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr ""

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Aktivní vlákno: %1"

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Současný rámec: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Hostitel: %1. Cíl: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr ""

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "Podpůrná vrstva DAP selhala"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "program skončil"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "vyžaduje odpojení"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "vyžaduje vypnutí"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Podpůrná vrstva DAP: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr ""

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr ""

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr ""

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(běží)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** spojení se serverem bylo zavřeno ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "program byl ukončen s kódem %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** čeká na činnost uživatele ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "chyba při odpovědi: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "důležité"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetrie"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "ladění procesu [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "ladění procesu [%1] "

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Zahájit metodu: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "vlákno %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "nastaven bod přerušení"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "odstraněn bod přerušení"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) bod přerušení"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr ""

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "možnosti serveru"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "podporováno"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "nepodporováno"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr ""

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr ""

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr ""

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr ""

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr ""

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr ""

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "chyba v syntaxi: výraz nenalezen"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "chyba v syntaxi: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "neplatný řádek: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "soubor nebyl určen: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "neplatné id vlákna: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr ""

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Dostupné příkazy:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "řádek %1 již má bod přerušení"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr ""

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Současné vlákno: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "žádný"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Současný rámec:"

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Stav sezení:"

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "probíhá inicializace"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "běžící"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "zastaveno"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "ukončeno"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "odpojen"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr ""

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "příkaz nenalezen"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "chybějící id vlákna"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr ""

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Současný rámec [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Hodnota"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "typ"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "indexované položky"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr ""

#: plugin_kategdb.cpp:106
#, kde-format
msgid "Kate Debug"
msgstr "Kate Debug"

#: plugin_kategdb.cpp:110
#, kde-format
msgid "Debug View"
msgstr "Ladicí pohled"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:343
#, kde-format
msgid "Debug"
msgstr "Ladit"

#: plugin_kategdb.cpp:113 plugin_kategdb.cpp:116
#, kde-format
msgid "Locals and Stack"
msgstr ""

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Číslo"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Rámec"

#: plugin_kategdb.cpp:200
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Ladicí výstup"

#: plugin_kategdb.cpp:201
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Nastavení"

#: plugin_kategdb.cpp:243
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""

#: plugin_kategdb.cpp:268
#, kde-format
msgid "Start Debugging"
msgstr "Začít ladění"

#: plugin_kategdb.cpp:278
#, kde-format
msgid "Kill / Stop Debugging"
msgstr ""

#: plugin_kategdb.cpp:285
#, kde-format
msgid "Continue"
msgstr "Pokračovat"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr ""

#: plugin_kategdb.cpp:297
#, kde-format
msgid "Step In"
msgstr ""

#: plugin_kategdb.cpp:304
#, kde-format
msgid "Step Over"
msgstr "Krok přes"

#: plugin_kategdb.cpp:311
#, kde-format
msgid "Step Out"
msgstr "Krok ven"

#: plugin_kategdb.cpp:318 plugin_kategdb.cpp:350
#, kde-format
msgid "Run To Cursor"
msgstr "Spustit ke kurzoru"

#: plugin_kategdb.cpp:325
#, kde-format
msgid "Restart Debugging"
msgstr ""

#: plugin_kategdb.cpp:333 plugin_kategdb.cpp:352
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Přesunout PC"

#: plugin_kategdb.cpp:338
#, kde-format
msgid "Print Value"
msgstr "Vypsat hodnotu"

#: plugin_kategdb.cpp:347
#, kde-format
msgid "popup_breakpoint"
msgstr ""

#: plugin_kategdb.cpp:349
#, kde-format
msgid "popup_run_to_cursor"
msgstr ""

#: plugin_kategdb.cpp:431 plugin_kategdb.cpp:447
#, kde-format
msgid "Insert breakpoint"
msgstr ""

#: plugin_kategdb.cpp:445
#, kde-format
msgid "Remove breakpoint"
msgstr ""

#: plugin_kategdb.cpp:599 plugin_kategdb.cpp:613
#, kde-format
msgid "Execution point"
msgstr ""

#: plugin_kategdb.cpp:771
#, kde-format
msgid "Thread %1"
msgstr "Vlákno %1"

#: plugin_kategdb.cpp:871
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:956 plugin_kategdb.cpp:964
#, kde-format
msgid "Breakpoint"
msgstr "Bod přerušení"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "La&dit"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Ladicí modul"

# Malayalam translations for kate package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kate package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-11 00:50+0000\n"
"PO-Revision-Date: 2018-08-16 08:58+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: plugin_katexmlcheck.cpp:95
#, kde-format
msgid "XML Check"
msgstr ""

#: plugin_katexmlcheck.cpp:100
#, kde-format
msgid "Validate XML"
msgstr ""

#: plugin_katexmlcheck.cpp:130
#, kde-format
msgid "Validate process crashed"
msgstr ""

#: plugin_katexmlcheck.cpp:130 plugin_katexmlcheck.cpp:148
#: plugin_katexmlcheck.cpp:208 plugin_katexmlcheck.cpp:233
#: plugin_katexmlcheck.cpp:253 plugin_katexmlcheck.cpp:341
#, kde-format
msgid "XMLCheck"
msgstr ""

#: plugin_katexmlcheck.cpp:144
#, kde-format
msgid "No DOCTYPE found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:146
#, kde-format
msgctxt "%1 refers to the XML DTD"
msgid "'%1' not found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:232
#, kde-format
msgid "<b>Error:</b> Could not create temporary file '%1'."
msgstr ""

#: plugin_katexmlcheck.cpp:251
#, kde-format
msgid ""
"<b>Error:</b> Failed to find xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""

#: plugin_katexmlcheck.cpp:339
#, kde-format
msgid ""
"<b>Error:</b> Failed to execute xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""

#. i18n: ectx: Menu (xml)
#: ui.rc:6
#, kde-format
msgid "&XML"
msgstr ""
